<?php
/*
Plugin Name: Wordpress Image Cleanup
Version: 0.1
Plugin URI: http://www.zoromski.com
Description: Check for uploaded images being used in posts
Author: Brian Zoromski

0.1
initial version

LICENSE

wp-image-cleanup.php
Copyright (C) 2020 Brian Zoromski
*/
function add_wp_image_cleanup() {
  // Add a new menu under Manage:
  add_management_page('Image Cleanup', 'Image Cleanup', 10, __FILE__, 'image_cleanup_menu');
}

add_action('admin_menu', 'add_wp_image_cleanup');

function is_image_in_posts( $image_name )
{
    global $wpdb;
    $value = '%'.like_escape( $image_name ).'%';
    $blog_id = get_current_blog_id();
    $wpdb->set_blog_id( $blog_id );
    $image = $wpdb->get_var( 
        $wpdb->prepare( 
            "SELECT ID FROM {$wpdb->posts} 
            WHERE  post_status = 'publish' 
            AND post_content LIKE '%s'"
            , $value 
        ) 
	);
	if (is_null($image)) {
		$image = $wpdb->get_var( 
			$wpdb->prepare( 
				"SELECT ID FROM {$wpdb->posts} 
				WHERE  post_type = 'attachment' 
				AND guid LIKE '%s'"
				, $value 
			) 
		);
	}
    return is_null( $image ) ? false : true;
}

function check_for_deleted_files(){
	$uploads = wp_get_upload_dir();
	$filename = "images.csv";
	$uploadfile = $uploads['basedir'] . '/' . $filename;
	$out = fopen($uploadfile, 'w');
	$line = "Filename\n";
	fputs($out, $line);
	
	$dir = new RecursiveDirectoryIterator($uploads['basedir']);

	$files = new RecursiveCallbackFilterIterator($dir, function ($current, $key, $iterator) {
		// Allow recursion
		if ($iterator->hasChildren()) {
			return TRUE;
		}
		if ($current->isFile() ) {
			return TRUE;
		}
		return FALSE;
	});
		
	foreach (new RecursiveIteratorIterator($files) as $file) {
		$path = $file->getPathname();
		if (strpos($path, ".png") || strpos($path, ".jpg") || strpos($path, ".gif") || strpos($path, ".pdf")){
			$length = strlen($path);
			$uploads_start = strpos($path, '/uploads');
			$short_path = substr($path, $uploads_start, $length-$uploads_start);
			if (!is_image_in_posts($short_path)) { 
				// echo "<b>Not </b> ";
				$file_array[] = $short_path;
				$line = $uploads["baseurl"] . substr_replace($short_path, '', 0, 8) . "\n";
				fputs($out, $line);
			}
		}
	}
	fclose($out);
	
	if (empty($file_array)) {
		return FALSE;
	} else {
		return TRUE;
	}
}

function image_cleanup_menu() 
{
	if (isset($_POST['step'])) {
		$step = intval($_POST["step"]);
		}
	else {
		$step = 0;
		}
		
	$error = false;
		
	echo "<STYLE type=\"text/css\">
		  div.float { float: left; }
		  div.step { float: left; font-weight:bold; }
		  div.next { border-style:solid; border-width:1px; padding: 1em;}
		  div.inactive { border-style:solid; border-width:1px; background-color: #666666; padding: 1em; }
		  div.active { border-style:solid; border-width:1px; background-color: #33FF00; padding: 1em; }
		  div.spacer { clear: both; }
		  div.status { float: center; background-color:#0000FF; color:#FFFFFF; font-weight:bold; padding: 1em; }
		  </STYLE>";
	echo "<div class=\"status\">";	
	switch ($step) {
		case 0:
			break;
		case 1:
			$error = check_for_deleted_files();
			break;
		case 2:

			break;
	}

		if (!$error) {
			$step = $step + 1;
			echo "<P>You're ready for Step #" . $step . "!<P>";
		}
		// else {
			// $step = $step - 1;
		// }
		echo "</div>";
		echo "<div class=\"spacer\"> &nbsp; </div>";
		
		echo "<div class=\"";
		if ($step < 2) {
			echo "active";
		}
		else {
			echo "inactive";
		}
		echo"\" >
		<div class=\"spacer\"> &nbsp; </div>
		<div class=\"step\"> Step 1:</div> <br>
		<div class=\"float\"> Check for unused images.</div>";
		if ($step === 1) {
			echo "<div class=\"float\"> <form enctype=\"multipart/form-data\" action=\"\" method=\"POST\">
			<input type=\"hidden\" name=\"step\" value=\"1\" />
			<input type=\"submit\" value=\"Check for Unused Images\" />
			</form></div>";
			}
		echo "<div class=\"spacer\"> &nbsp; </div>
		</div>
		<div class=\"";
		if ($step === 2) {
			echo "active";
		} elseif ($step < 2) {
			echo "next";
		} elseif ($step > 2) {
			echo "inactive";
		}
		$uploads = wp_get_upload_dir();
		$filename = "images.csv";
		$uploadfile = $uploads['basedir'] . '/' . $filename;
		echo "\" >
			<div class=\"spacer\"> &nbsp; </div>
			<div class=\"step\"> Step 2:</div> <br>
			<div class=\"float\"> Now, <a href=\"" . $uploadfile . "\"><B>right click on this link</B></a> and choose \"Save Link As...\" to download the list of images that are not used.<P>
			</div>";
		echo "<div class=\"spacer\"> &nbsp; </div>
		</div>";
	
}	


?>